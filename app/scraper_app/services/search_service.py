import datetime
import json
from typing import Optional

from .keywords_counter import KeywordsCounter
from .scraper_service import ScraperService
from ..models import Search, Result


def get_search_object_newer_than(phrase: str, time_delta_seconds: int) -> Optional[Search]:
    db_time = datetime.datetime.now() - datetime.timedelta(seconds=int(time_delta_seconds))
    return Search.objects.filter(created_at__gte=db_time, phrase=phrase).first()


def new_search(phrase, ip) -> Search:
    scraper = ScraperService()
    results_count, results = scraper.scrape(phrase)

    keywords_counter = KeywordsCounter()
    popular_keywords = keywords_counter.get_most_popular_keywords(results)
    json_dumps = json.dumps(popular_keywords)

    phrase_data = {
        'phrase': phrase,
        'ip': ip,
        'results_count': results_count,
        'popular_keywords': json_dumps
    }
    search = Search.objects.create(**phrase_data)

    for result in results:
        Result.objects.create(**result, search=search)

    return search
