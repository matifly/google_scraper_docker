class KeywordsCounter:
    __keywords = {}

    def get_most_popular_keywords(self, results, count=10):
        keywords = self.get_all_keywords(results)
        most_popular_keywords = sorted(keywords.items(), key=lambda kv: (kv[1], kv[0]), reverse=True)[:count]
        return most_popular_keywords

    def get_all_keywords(self, results):
        self.__keywords = {}
        for result in results:
            self.__add_keywords_from_result(result)
        return self.__keywords

    def __add_keywords_from_result(self, result):
        splitted = result['title'].split() + result['description'].split()
        for splitted_el in splitted:
            self.__add_to_keword_if_allowed(splitted_el)

    def __add_to_keword_if_allowed(self, keyword: str):
        stripped_el = self.__strip_string(keyword).lower()
        if len(stripped_el) > 3:
            if (stripped_el in self.__keywords):
                self.__keywords[stripped_el] += 1
            else:
                self.__keywords[stripped_el] = 1

    def __strip_string(self, string):
        return ''.join(filter(str.isalpha, string))
