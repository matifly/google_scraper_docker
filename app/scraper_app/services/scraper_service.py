from selenium import webdriver


class ScraperService:

    def __init__(self):
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        options.add_argument('no-sandbox')
        self.browser = webdriver.Chrome(options=options)


    def scrape(self, phrase):
        phrase = phrase.replace(" ", "+")
        url = self.__get_url(phrase)
        self.browser.get(url)
        dom_results = self.browser.find_elements_by_xpath(
            "//div[@class='g']//*[not(ancestor::*[@class='xpdopen'])]//div[@class='rc']")
        results_count_object = self.browser.find_element_by_id("resultStats")
        results_count = self.__extract_number_from_string(results_count_object.text)
        results = []

        for index, result in enumerate(dom_results):
            try:
                title = result.find_element_by_tag_name("h3")
                description = result.find_element_by_class_name("s").find_element_by_tag_name("span")
                link = result.find_element_by_tag_name("a")
            except:
                title = None
                description = None
                link = None

            results.append({
                'index': index,
                'title': title.text if title else '',
                'description': description.text if description else '',
                'link': link.get_attribute('href') if link else ''
            })

        return results_count, results

    def __get_url(self, phrase):
        return f"https://www.google.pl/search?hl=pl&q={phrase}&og={phrase}"

    def __extract_number_from_string(self, string):
        numbers_arr = [s for s in string.split() if s.isdigit()]
        return int("".join(numbers_arr))
